import express from "express"
import { config } from "dotenv"
import cors from "cors"
import { sendEmail } from "./utils/sendEmail.js"

const app=express()

const router=express.Router()

//console.log(process.env.PORT)

//env path config
config({path:"./config.env"})

//cors
app.use(cors())

//middle wares
app.use(express.json())

app.use(router)

app.use(express.urlencoded({extended:true}))


router.post("/send/mail",async(req,res,next)=>{
    const{name,email,message}=req.body;

    if(!name || !email || !message){
        return next(
            res.status(400).json({
                success:false,
                message:"Please provide all details"
            })
        )
    }

    try{
        await sendEmail({
            email:"mern@gmail.com",
            subject:"GYM WEBSITE CONTACT",
            message,
            userEmail:email,
        })

        res.status(200).json({
            success:true,
            message:"Message sent successfulyy"
        })
    }

    catch(err){
       res.status(500).json({
        success:false,
        message:"Message sent failed, something went wrong"
       })
    }



})

//port
app.listen(process.env.PORT,()=>{
    console.log(`server listening at port ${process.env.PORT}`)
})


//welcome messge
router.get("/",(req,res)=>{
    res.json({success:true,
        message:"Hello welcome"
    })
})