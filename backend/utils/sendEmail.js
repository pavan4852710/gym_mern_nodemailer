
import nodemailer from "nodemailer";

export const sendEmail=async(options)=>{
    const transporter = nodemailer.createTransport({

        host: process.env.SMTP_HOST,
        port: process.env.PORT,
        service:process.env.SMTP_SERVICE,
        secure: false, // Use `true` for port 465, `false` for all other ports
        auth: {
          user: process.env.SMTP_MAIL,
          pass: process.env.SMTP_PASSWORD,
        },

})

const mailOptions={
    from:process.env.SMTP_MAIL,
    to:options.email,
    subject:options.subject,
    text:`${options.message} \n\n Email of user who sen the message : ${options.userEmail}`
}

await transporter.sendMail(mailOptions)
}