import React from 'react'
import {BrowserRouter as Router} from "react-router-dom"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Contact from './components/Contact';
import Navbar from './components/Navbar';
import BMICalculator from './components/BMICalculator';
import WorkoutSessions from './components/WorkoutSessions';
import Footer from './components/Footer';
import Gallery from './components/Gallery';
import Hero from './components/Hero';
import Pricing from './components/Pricing';
import "./App.css"
const App = () => {
  return (
    <Router>
      <Navbar/>
      <Hero/>
      <WorkoutSessions/>
      <Gallery/>
      <Pricing/>
      <Contact/>
      <BMICalculator/>
      <Footer/>
      <ToastContainer theme='dark' position='top-center'/>
    </Router>
    
  )
}

export default App