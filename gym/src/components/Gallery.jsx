import React from 'react'

const Gallery = () => {
  const gallery=["/img1.webp","/img2.jpg","/img3.jpg","/img7.jpg","/img3.jpg","/img2.jpg","/img1.webp","/img2.jpg"]
  return (
    <section className="gallery">
      <h1>BETTER BEATS BEST</h1>
      <div className="images">
      <div>
        {
          gallery.slice(0,3).map((element,index)=>{
            return <img src={element} key={index} alt="gallery images" />
          })
          
        }
      </div>

      <div>
        {
          gallery.slice(3,6).map((element,index)=>{
            return <img src={element} key={index} alt="gallery images" />
          })
          
        }
      </div>

      <div>
        {
          gallery.slice(6,9).map((element,index)=>{
            return <img src={element} key={index} alt="gallery images" />
          })
          
        }
      </div></div>
    </section>
  )
}

export default Gallery