import React from 'react'
import {Check} from "lucide-react"
import {Link} from "react-router-dom"
const Pricing = () => {

  const pricing=[
    {
      imgUrl:"/pricing.jpg",
      title:"Quaterly",
      price:18000,
      length:3
    },
    {
      imgUrl:"/pricing.jpg",
      title:"Half Yearly",
      price:34000,
      length:6
    }
    ,
    {
      imgUrl:"/pricing.jpg",
      title:"Annual Year",
      price:40000,
      length:12
    }
  ]
  return (
    <>
      <section className="pricing">
        <h1>ELITE EDGE PLANS</h1>
        <div className="wrapper">
          {
            pricing.map((element,index)=>{
              return <div className="card" key={index}>
                <img src={element.imgUrl} alt={element.title} />
                <div className="title">
                    <h1>{element.title}</h1>
                    <h1>PACKAGE</h1>
                    <h1>rs {element.price}</h1>
                    <p>For {element.length} Months</p>
                </div>

                <div className="description">
                  <p>
                    <Check/> Equipment
                  </p>
                  <p>
                    <Check/> All Day Free Training
                  </p>
                  <p>
                    <Check/> Free Restroom
                  </p>
                  <p>
                    <Check/> 24/7 support
                  </p>
                  <Link to={"/"}>JOIN NOW</Link>
                </div>
              </div>
            })
          }
        </div>
      </section>
    </>
  )
}

export default Pricing