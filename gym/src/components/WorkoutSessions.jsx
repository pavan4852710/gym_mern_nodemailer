import React from 'react'

const WorkoutSessions = () => {
  return (
    <section className="workout_session">
      <div className='wrapper'>
        <h1>TOP WORKOUT SESSION</h1>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem 
        Ipsum has been the industry's standard dummy text ever since the 1500s.
        </p>
        <img src="/img5.jpg" alt="" />
      </div>
      <div className="wrapper">
        <h1>FEATURED BOOTCAMPS</h1>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem 
        Ipsum has been the industry's standard dummy</p>
      
      <div className="bootcamps">
        <div>
          <h4>WEIGHT LOSS</h4>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem 
        Ipsum has been the industry's standard dummy</p>
        </div>

        <div>
          <h4>ZUMBA</h4>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem 
        Ipsum has been the industry's standard dummy</p>
        </div>

        <div>
          <h4>INTENSE STRENGTH</h4>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem 
        Ipsum has been the industry's standard dummy</p>
        </div>


        <div>
          <h4>BASIC EXERCISE</h4>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem 
        Ipsum has been the industry's standard dummy</p>
        </div>
      </div>
      </div>
    </section>
  )
}

export default WorkoutSessions