import React, { useState } from 'react'
import {toast} from "react-toastify"
const BMICalculator = () => {
  const[height,setHeight]=useState("")
  const[weight,setWeight]=useState("")
  const[gender,setGender]=useState("")
  const[bmi,setBmi]=useState("")


  const calculateBmi=(e)=>{
    e.preventDefault()

    if(!height || !weight || !gender){
      toast.error("please enter valid height,weight,gender")
      return
    }

    const heightInMeters=height/100;
    const bmiValue=(weight/(heightInMeters*heightInMeters)).toFixed(2);
    setBmi(bmiValue)


    if(bmiValue<18.5){
      toast.warning("You are under weight. Consider seeking  advice from healthcare provider.")
    }

    if(bmiValue>=18.5 && bmiValue<24.9){
      toast.success("You have normal weight. Keep maintain a healthy lifestyle")
    }

    else  if(bmiValue>=25 && bmiValue<29.9){
      toast.warning("You are over weight. Consider seeking  advice from healthcare provider.")
    }

    else{
      toast.warning("You are obese range. Consider seeking  advice from healthcare provider.")

    }
  }
  return (
  <section className='bmi'>
    <h1>BMI calculator</h1>
    <div className="container">
      <div className="wrapper">
        <form onSubmit={calculateBmi}>
          <div>
            <label>Height(cm)</label>
            <input type="text" value={height} onChange={(e)=>setHeight(e.target.value)}/>
          </div>

          <div>
            <label>Weight(kg)</label>
            <input type="text" value={weight} onChange={(e)=>setWeight(e.target.value)}/>
          </div>

          <div>
            <select value={gender} onChange={(e)=>setGender(e.target.value)}>
              <option value="">Select Gender</option>
              <option value="male">Male</option>
              <option value="">Female</option>
            </select>
          </div>
          <button type='submit'>Calculate BMI</button>
        </form>
      </div>
      <div className="wrapper">
        <img src="/bmi.jpg" alt="" />
      </div>
    </div>
  </section>
  )
}

export default BMICalculator